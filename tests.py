#!/usr/bin/env python
# -*- coding: utf-8 -*-
## tests.py for  in /home/wapiflapi/Projects/wd
##
## Made by Wannes Rombouts
## Login   <rombou_w@epitech.net>
##
## Started on  Mon Apr  1 20:22:17 2013 Wannes Rombouts
## Last update Tue Apr  2 02:41:39 2013 Wannes Rombouts
##

import wdaemon
import time
import argparse

class TestClass(object):

    def __init__(self, data):
        self.data = data

    def test(self, name):
        return "%s -> %s" % (self.data, name)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--daemon",
                        help="Starts a %(prog)s daemon.")
    args = parser.parse_args()

    daemon = wdaemon.UnixProxyDaemon("/tmp/test_pid", "/tmp/test_sock", TestClass("testing"))

    if args.daemon == "start":
        daemon.start(stdout=open("/tmp/stdout", "w"), stderr=open("/tmp/stderr", "w"))
    elif args.daemon == "stop":
        daemon.stop()
    elif args.daemon == "restart":
        daemon.restart(stdout=open("/tmp/stdout", "w"), stderr=open("/tmp/stderr", "w"))

    if args.daemon != None:
        print "daemon action done."
        exit(0)

    proxy = wdaemon.UnixProxyClient("/tmp/test_sock", TestClass)
    print proxy.test("wapiflapi")
