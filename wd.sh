#!/bin/sh
## wd.sh for  in /home/wapiflapi
##
## Made by Wannes Rombouts
## Login   <wapiflapi@epitech.net>
##
## Started on  Mon Apr  1 15:21:02 2013 Wannes Rombouts
## Last update Fri Apr 19 18:41:03 2013 Wannes Rombouts
##

# wd.py should be in your path. It wd's backend. wd is just a bash function defined here.
# If you do not wish do have it in your PATH, you can also set the WDBIN variable before
# the inclusion of this script to the script's path.
: ${WDBIN:="wd.py"}

# This configuration can be altered if you define it before the inclusion of this script.
: ${WD_MAX_AUTOCOMP:=10}
: ${WD_MAX_CACHE:=10}

function wdl () {
    "$WDBIN" "-L"
}

function wd () {
    local target
    target=$("$WDBIN" --conservative --cache "$WD_MAX_CACHE" "$@")

    if [[ "$target" ]]; then
	builtin cd "$target" > /dev/null
    else
	builtin cd "$@" > /dev/null
    fi

    # Set WD_TARGET so that scripts can know if we jumped or not.
    if [[ $? -eq 0 ]]; then
	WD_TARGET="$PWD"
    else
	WD_TARGET=""
    fi

    return $?
}

_wd ()
{
  COMPREPLY=()

  # First if we are not autocompleting try normal autocompletion.
  if [ ${#COMP_WORDS[@]} -eq 2 ] &&
      ! "$WDBIN" -D --is-autocompleting "${COMP_WORDS[@]:1}" >& /dev/null; then

      echo "wd said it was not currently autocompleting. Lets try simple matches first." >> /tmp/shout

      compgen -d "${COMP_WORDS[1]}" 2> /dev/null >> /tmp/shout

      # What the hell is the proper way to check if something returned data?
      # Any way, if ther is a possible autocompletion we just return nothing.
      # complete should handle it for us.
      if [[ $(compgen -d "${COMP_WORDS[1]}" 2> /dev/null) ]]; then
	  return 0
      fi

  fi

  # Ask wd for sugestions.
  readarray -t COMPREPLY < <("$WDBIN" -D --autocomplete --list "$WD_MAX_AUTOCOMP" "${COMP_WORDS[@]:1}" 2> /dev/null)

  # If wd returns only one match run directory autcompletion on it.
  if [ ${#COMPREPLY[@]} -eq 1 ]; then
      path=${COMPREPLY[0]}

      echo "wd returned only $path ... autcompleting." >> /tmp/shout

      readarray -t COMPREPLY < <(compgen -d "$path" 2> /dev/null)

      # If the user won't be given a choice after this, simply keep wd's output.
      if [ ${#COMPREPLY[@]} -lt 2 ]; then
	  COMPREPLY[0]=$path
      fi
  fi

  return 0
}

complete -F _wd -o dirnames wd.py wd
