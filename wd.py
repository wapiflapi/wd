#!/usr/bin/env python
# -*- coding: utf-8 -*-
## wd.py for  in /home/wapiflapi
##
## Made by Wannes Rombouts
## Login   <wapiflapi@epitech.net>
##
## Started on  Mon Apr  1 13:02:54 2013 Wannes Rombouts
## Last update Sat Apr 27 22:04:16 2013 Wannes Rombouts
##

import os
import sys
import time
import argparse
import itertools

import collections
import wdaemon
import operator

import Levenshtein
import blist

PRINT_DEBUG=True

def debug(*args, **kwargs):
    if PRINT_DEBUG:
        sys.stderr.write((" ".join(x.encode(sys.stdin.encoding, 'replace') for x in args) + "\n"), **kwargs)

def set_debug(state):
    global PRINT_DEBUG
    PRINT_DEBUG = state

class HintCache(object):
    def __init__(self, expected_prefix, stack):
        self.expected_prefix = expected_prefix
        self.stack = stack

    def __repr__(self):
        return "len(cache(%s)) == %d" % (self.expected_prefix, len(self.stack))

    def filter(self, new_prefix):
        if not new_prefix.startswith(self.expected_prefix):
            raise ValueError("Asked to filter a Stack on '%s', but expected at least '%s'" %
                             (new_prefix, self.expected_prefix))

        new_stack = type(self.stack)()
        for confidence, path in self.stack.iterconfidence():
            if path.startswith(new_prefix):
                new_stack.add(confidence, path)

        self.expected_prefix = new_prefix
        self.stack = new_stack


class ConfidenceStack(object):
    """A Stack represents an ordered set of paths.
    It is sorted according to (confidence, insertion_time)
    Also any path can only be in the stack once."""

    def __init__(self, iterable=()):
        self._paths = {}
        self._confidence = blist.sortedset(key=lambda x: (-x[0][0], -x[0][1]))
        self._counter = 0
        for x in iterable:
            self.add(*x)

    def __len__(self):
        return len(self._paths)

    def __bool__(self):
        return len(self) != 0

    def __getitem__(self, i):
        if isinstance(i, slice):
            return [self[ii] for ii in xrange(*i.indices(len(self)))]
        return self._confidence[i][1]

    def __iter__(self):
        return (x[1] for x in self._confidence)

    def __reversed__(self):
        return (x[1] for x in reversed(self._confidence))

    def __repr__(self):
        return '%s(%r)' % (self.__class__.__name__, self._confidence)

    def __contains__(self, item):
        return item in self._paths

    def __counter(self):
        self._counter += 1
        return self._counter

    def iterconfidence(self):
        return ((x[0][0], x[1]) for x in self._confidence)

    def confidence(self):
        return self._confidence

    def index(self, item):
        return self._confidence.index(self._paths[item])

    def add(self, confidence, path):
        path_entry = self._paths.get(path)
        if path_entry:
            confidence = max(confidence, path_entry[0][0])
            self._confidence.remove(path_entry)

        path_entry = ((confidence, self.__counter()), path)
        self._paths[path] = path_entry
        self._confidence.add(path_entry)


class Wd(object):

    def __init__(self, db_file):
        self.predict_prefix = self.predict_prefix_bash
        self.db_file = db_file
        self.lastsave = 0
        self.loaddb()
        self.reset()

    def reset(self):
        self.expected_prefix = ""
        self.current_hints = []
        self.cache_access = 0
        self.max_stack_size = 1000
        self.max_shell_size = 1000
        # Those values are still beeing tuned.
        self.true_confidence = 0.85
        self.min_confidence  = 0.25

    def loaddb(self):
        self.global_stack = ConfidenceStack((1.0, path[:-1].decode("unicode-escape")) for path in self.db_file)
        debug("Loaded %d entries from %s" % (len(self.global_stack), self.db_file.name))

    def savedb(self):
        try:
            self.db_file.close()
        except:
            pass
        self.db_file = open(self.db_file.name, "w")
        self.db_file.writelines(path.encode("unicode-escape") + "\n" for path in reversed(self.global_stack))
        self.db_file.close()
        debug("Wrote %d entries to %s" % (len(self.global_stack), self.db_file.name))
        self.lastsave = time.time()

    def _cache_hint(self, expected_prefix, stack):
        self.current_hints.append(HintCache(expected_prefix, stack))

    def split_path(self, path):
        """Splits a path into its components."""

        if not path:
            return []

        path = os.path.normpath(path)
        components = []
        while True:
            path, folder = os.path.split(path)
            if folder:
                components.append(folder)
            else:
                if path:
                    components.append(path)
                break
        return components[::-1]

    def match(self, ref, hint):

        refl = ref.lower()
        hintl = hint.lower()
        lev = max(Levenshtein.ratio(ref, hint), Levenshtein.ratio(refl, hintl))

        if refl.startswith(hintl) or refl.endswith(hintl):
            lev = 1 - (1 - lev) ** 4
        elif hintl in refl:
            lev = 1 - (1 - lev) ** 2

        return lev

    def smart_filter_yield(self, stack, hint):
        """Yields a (confidence, path) tupple for the next path that
        we think matches the hint."""

        # When matching a 'parent' and a 'child' we have offsets: [len(parent) - len(child), len(parent)]
        # overlaping positions. The confidence of each is the sum of the matching
        # confidence of each pair.
        # confidence(P, C, off) == sum(match(p, c) for p, c in zip(P[off], C))

        hint_parts = self.split_path(hint)

        for old_confidence, path in stack.iterconfidence():
            path_parts = self.split_path(path)

            # debug("Considering %s" % path_parts)

            # If this is not the global stack we want to be more relaxed
            # and allow for easier matches. In particular we want to allow
            # "/a/b/my/filter/c/d" to match the hint "my/filter"
            if stack != self.global_stack:
                start_overlay = 0
            else:
                start_overlay = max(0, (len(path_parts) - len(hint_parts)))

            for overlay in xrange(len(path_parts)-1, start_overlay-1, -1):

                new_path = os.path.join(*itertools.chain(path_parts,
                                                         hint_parts[len(path_parts)-overlay:]))

                if not os.path.isdir(new_path):
                    continue

                confidence_parts = [self.match(p, c) ** 2 for p, c in itertools.izip(path_parts[overlay:], hint_parts)]
                confidence = reduce(operator.mul, confidence_parts)

                if confidence > self.min_confidence:
                    debug("\tConfidence: %s, %s, %s" % (confidence, confidence_parts, new_path))
                    yield (confidence * old_confidence, new_path)
                    break

    def smart_filter(self, stack, hint):
        """Returns a filtered copy of the stack which contains only entries matching
        the hint. Also stores it in the cache for future use."""

        _sugestions = ConfidenceStack()
        nb_good_enough = 0

        for confidence, path in self.smart_filter_yield(stack, hint):
            if confidence >= self.true_confidence:
                nb_good_enough += 1
                confidence = 1.0

            _sugestions.add(confidence, path)

            if nb_good_enough >= self.max_stack_size:
                break

        # Great. Now the sugestions are reversed -_-
        # lets put them back.
        sugestions = ConfidenceStack()
        for confidence, path in _sugestions.iterconfidence():
            sugestions.add(confidence, path)

        # The expected prefix computed here needs to be computed according to
        # the shell's point of view. There is some tricky stuff going on here,
        # because the shell might replace the user's input with a common prefix
        # deduced from a sub-set of the actual stack. There is no user-friendly
        # way to allow the user to know this.
        # - If he just keeps the hint as-is: *Fine*, the rest of the filters will
        #   apply to the whole set.
        # - If he adds stuff to the filter (standart autocompletion for example)
        #   the stack will be recalculated and the old 'extra' paths will be lost.
        #   To bad. Anyone has any ideas ?
        # Note to my future self: I *did* try to think about this.
        expected_prefix = self.predict_prefix(hint, itertools.islice(sugestions, self.max_shell_size))
        self._cache_hint(expected_prefix, sugestions)

        return sugestions

    def deduce_cache_status(self, hints, auto_autocomplete=False):
        """The goal is to have diff_idx beeing the smalest possible index where current_hints
        and hints differ. If one list is shorter than the other diff_idx will be left to
        len(shortest_list). autocompletion indicates if the diference is solvable with a
        simple filter

        returns: (diff_idx, autocomplete)"""

        diff_idx = -1
        for diff_idx in xrange(min(len(self.current_hints), len(hints))):
            if self.current_hints[diff_idx].expected_prefix != hints[diff_idx]:
                break
        else:
            diff_idx += 1

        autocomplete = False

        # If the diff can be recovered with simple autocompletion style filtering do it:
        if diff_idx < len(hints) and diff_idx < len(self.current_hints) and len(self.current_hints[diff_idx].stack):
            if hints[diff_idx].startswith(self.current_hints[diff_idx].expected_prefix):
                if auto_autocomplete:
                    debug("\t[+] Using simple autocompletion (started with %s)" % (
                            self.current_hints[diff_idx].expected_prefix))
                    self.current_hints[diff_idx].filter(hints[diff_idx])
                    diff_idx += 1
                else:
                    autocomplete = True

        # Maybe we didnt autocomplete because nothing changed. (Everything passed)
        if not autocomplete:
            autocomplete = diff_idx == len(self.current_hints) and any(h.stack for h in self.current_hints)

        debug("[*] Hints diff with cache at %d (%s vs %s) (autocompletion=%s)" % (
                diff_idx, hints, self.current_hints, autocomplete))

        return diff_idx, autocomplete

    def sugest(self, hints, nb_sugestions=1):
        """Generate sugestions based on hints.
        Basicaly this is reduce(smart_filter, hints, global_stack)
        This is highly optimized using caching to better handle auto-completion.

        returns: An iterator on a ConfidenceStack"""

        # Let the internals know how many paths the shell will see.
        self.max_stack_size = nb_sugestions

        # The goal is to have diff_idx beeing the smalest possible index where current_hints
        # and hints differ. If one list is shorter than the other diff_idx will be left to
        # len(shortest_list)

        diff_idx, autocomplete = self.deduce_cache_status(hints, auto_autocomplete=True)

        # Remove covered hints:
        del hints[:diff_idx]
        # Remove bad cache entries
        del self.current_hints[diff_idx:]

        starting_stack = self.current_hints[-1].stack if self.current_hints else self.global_stack
        sugestions = reduce(self.smart_filter, hints, starting_stack)

        return itertools.islice(sugestions, nb_sugestions)

    def predict_prefix(self, hint, stack):
        """This should be overwritten, for example by on of the predefined predict_prefix_*
        functions. The purpose of this function is to predict how the shell will alter the
        user's input after we return the stack as auto-completion candidates."""
        raise NotImplementedError(predict_prefix.__doc__)

    def predict_prefix_bash(self, hint, stack):
        """Bash will replace the hint with the common prefix of the stack unless
        that is an empty string in which case the hint will be left as is."""

        if not stack:
            return hint
        prefix = os.path.commonprefix(list(stack))
        return prefix if prefix else hint

    def real_dir(self, head, tail=""):
        path = os.path.expanduser(head)
        if tail:
            path = os.path.join(path, os.path.expanduser(tail))
        return path

    #
    # Start of functions called by the main program
    #

    def autocomplete_running(self, cwd, hints):
        _, autocomplete = self.deduce_cache_status(hints)
        return autocomplete

    def find_directory(self, cwd, hints, nb_sugestions):

        self.cache_access = 0

        debug("*"*40)
        debug("[*] Finding dirs for %s, stop at %d" % (hints, nb_sugestions))

        dirs = list(collections.OrderedDict.fromkeys(self.sugest(hints, nb_sugestions)))

        debug("[!] Done. Cache:")
        for c in self.current_hints:
            debug("\t%s" % c)
        debug("[>] Results:")
        for c in dirs:
            debug("\t%s" % c)

        return list(collections.OrderedDict.fromkeys(dirs))

    def change_directory(self, cwd, path):
        path = self.real_dir(cwd, path)
        if not os.path.isdir(path):
            debug("Seems [%s] thing isnt a path")
            return

        parts = self.split_path(path)
        debug("[@] Saving %s" % parts)
        for i in xrange(len(parts)):
            subpath = os.path.join(*parts[:i+1])
            self.global_stack.add(1.0, subpath)

        if time.time() - self.lastsave > 60*5:
            self.savedb()

    def consult_cache(self, cmd):

        if not self.current_hints or not self.current_hints[-1].stack:
            return []

        if cmd == None:
            cache = list(iter(self.current_hints[-1].stack)) if self.current_hints else []
            return cache

        if cmd == "":
            self.cache_access += 1
        elif cmd == "=":
            self.cache_access -= 1
        else:
            try:
                self.cache_access = int(cmd)
            except:
                return []

        # Make sure it is wihtin range:
        stack_size = len(self.current_hints[-1].stack)
        self.cache_access = (self.cache_access % stack_size + stack_size) % stack_size

        output = self.current_hints[-1].stack[self.cache_access]
        debug("consulting cache at %d / %d == %sx" % (self.cache_access, stack_size, output))

        return [output]

def start_daemon(db_file):
    daemon = wdaemon.UnixProxyDaemon("/tmp/test_pid", "/tmp/test_sock", Wd(db_file))
    daemon.start(stdout=open("/tmp/stdout", "w"), stderr=open("/tmp/stderr", "w"))

def stop_daemon():
    daemon = wdaemon.Daemon("/tmp/test_pid")
    daemon.stop()

def restart_daemon(db_file, mustrun=False):
    try:
        stop_daemon()
    except wdaemon.DaemonNotRunning:
        if mustrun:
            raise
    start_daemon(db_file)

def setup_wd(args, assert_daemon=False):
    wd = None

    if not args.daemon == "none":
        try:
            wd = wdaemon.UnixProxyClient("/tmp/test_sock", Wd)
            wd.testProxyClient()
        except wdaemon.DaemonNotRunning as e:
            wd = None

    # If at this point we are not connect to a daemon create a local instance:
    if wd == None and not assert_daemon:
        set_debug(args.debug)
        debug("Running on local instance.")
        wd = Wd(args.db_file)

    return wd

def remote_control(args):
    if args.daemon == "stop":
        try:
            stop_daemon();
        except wdaemon.DaemonNotRunning:
            debug("Daemon is not runing.")
            exit(1)
    elif args.daemon == "start":
        try:
            start_daemon(args.db_file);
        except wdaemon.DaemonRunning as e:
            debug("%s" % e)
            exit(1)
    elif args.daemon == "restart":
        restart_daemon(args.db_file);
    else:
        return False

    return True

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument("dir", nargs="*", type=str,
                        help="Hints to where you want to change your current working directory, every hint is applied to the set deduced from the previous one")

    parser.add_argument("-d", "--daemon", choices=["start", "stop", "restart", "none"],
                        help="Controls the %(prog)s daemon")
    parser.add_argument("-D", "--assert-daemon", action="store_true",
                        help="Ensures wd works with a daemon.")

    parser.add_argument("-f", "--db-file", type=argparse.FileType('ab+'),
                        default=os.path.expanduser("~/.wddb"),
                        help="File to use as a database")
    parser.add_argument("-s", "--save", action="store_true",
                        help="Save the database when done")
    parser.add_argument("--debug", action="store_true",
                        help="Show debug, even if not in demon mode.")

    parser.add_argument("-c", "--conservative", action="store_true",
                        help="If hints already indicate an existing directory dont try to guess")
    parser.add_argument("-a", "--autocomplete", action="store_true",
                        help="Don't leave the hints untouched, something must change, also do not remember the results")
    parser.add_argument("--is-autocompleting", action="store_true",
                        help="If this is specified wd will check if it could autocomplete using the current hints "
                        "and will exit with a non null status if it is not possible.")

    parser.add_argument("-l", "--list", nargs="?", type=int, default=1, const=10, metavar="N",
                        help="list %(metavar)s propositions.")
    parser.add_argument("-L", "--list-cache", action="store_true",
                        help="list the cache as used by =x hints")
    parser.add_argument("-k", "--cache", type=int, default=10, metavar="N",
                        help="Cache %(metavar)s propositions for use with =x hints.")

    args = parser.parse_args()

    # If we just want to control an existing daemon we exit() afterwards.
    if remote_control(args): exit()

    # Set up a Wd or proxy object.
    wd = setup_wd(args, args.assert_daemon)
    if not wd:
        exit(1)

    cwd, hints = os.getcwd(), [unicode(d, sys.stdin.encoding) for d in args.dir]

    output = []

    if args.conservative and not hints:
        hints = [os.path.expanduser("~")]

    # Optimisations might work localy on hints, save them.
    original_hints = hints[:]

    if args.is_autocompleting:
        exit(0 if hints and wd.autocomplete_running(cwd, hints) else 1)

    if args.list_cache:
        for i, p in enumerate(wd.consult_cache(None)):
            print i, p
        exit(0)

    if not hints:
        output = []
    elif hints[-1].startswith("="):
        output = wd.consult_cache(hints[-1][1:])
    elif os.path.isdir(os.path.expanduser(hints[-1])) and (args.conservative or not wd.autocomplete_running(cwd, hints)):
        output = hints[-1:]
    else:
        output = wd.find_directory(cwd, hints, max(args.list, args.cache))

    # handle results
    for proposition in itertools.islice(output, args.list):
        if not args.autocomplete:
            if proposition == output[0]:
                wd.change_directory(cwd, proposition)
        elif proposition == original_hints[-1]:
            continue
        else:
            proposition = os.path.join(proposition, "")
        print proposition.encode(sys.stdin.encoding, 'replace')

    if args.save:
        wd.savedb()
