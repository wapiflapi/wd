#!/usr/bin/env python
# -*- coding: utf-8 -*-
## wdaemon.py for  in /home/wapiflapi/Projects/wd
##
## Made by Wannes Rombouts
## Login   <rombou_w@epitech.net>
##
## Started on  Mon Apr  1 19:44:04 2013 Wannes Rombouts
## Last update Fri Apr  5 13:51:29 2013 Wannes Rombouts
##

import os
import sys
import time
import signal
import socket
import itertools
import json

class DaemonException(RuntimeError):
    pass

class DaemonNotRunning(DaemonException):
    def __init__(self, msg="Daemon is not running.", *args, **kwargs):
        DaemonException.__init__(self, msg, *args, **kwargs)

class DaemonRunning(DaemonException):
    def __init__(self, info=None, *args, **kwargs):
        if type(info) == int:
            msg = "Daemon is already running with pid %d" % info
        elif info == None:
            msg = "Daemon is already running."
        else:
            msg = info

        DaemonException.__init__(self, msg, *args, **kwargs)


class Daemon(object):
    """A generic daemon class.

    Usage: subclass the daemon class and override the run() method. \
    You can also override dying()."""

    def __init__(self, pidfile_path):
        self.pidfile = pidfile_path

    def __daemonize(self, stdin, stdout, stderr):

        # If this fails we can still raise the error.
        pid = os.fork()
        if pid > 0:
            sys.exit(0)

        # Now the parent should be dead. Too bad if we want to raise anything :(
        os.chdir('/')
        os.setsid()
        os.umask(0)

        # Do second fork.
        try:
            pid = os.fork()
            if pid > 0: sys.exit(0)
        except OSError as err:
            sys.exit("Second fork failed (%s), could not daemonize." % err)

        # redirect standard file descriptors

        sys.stdout.flush()
        sys.stderr.flush()

        stdin  = open(os.devnull, 'r')  if stdin  is None else stdin
        stdout = open(os.devnull, 'a+') if stdout is None else stdout
        stderr = open(os.devnull, 'a+') if stderr is None else stderr

        os.dup2(stdin.fileno(), sys.stdin.fileno())
        os.dup2(stdout.fileno(), sys.stdout.fileno())
        os.dup2(stderr.fileno(), sys.stderr.fileno())

        print "All dups done. (%s, %s, %s)" % (stdin, stdout, stderr)

        # write pidfile, and make sure it will be removed upon exit
        import atexit
        atexit.register(self.dying)

        pid = str(os.getpid())
        with open(self.pidfile,'w+') as f:
            f.write(pid + '\n')

    def __getpid(self):
        with open(self.pidfile,'r') as pf:
            return int(pf.read().strip())

    def __delpid(self):
        os.remove(self.pidfile)

    def stop(self):
        try:
            pid = self.__getpid()
        except IOError:
            raise DaemonNotRunning()

        try:
            while True:
                os.kill(pid, signal.SIGTERM)
                time.sleep(0.1)
        except OSError as err:
            if err.strerror == "No such process":
                self.__delpid();
            else:
                raise DaemonException(err)

        # give the daemon time to go away.
        time.sleep(0.5)

    def start(self, stdin=None, stdout=None, stderr=None):
        # Check for a pidfile to see if the daemon already runs
        try:
            pid = self.__getpid()
        except IOError:
            self.__daemonize(stdin, stdout, stderr)
            self.__run()
        else:
            raise DaemonRunning(pid)

    def restart(self, require_stop=False, stdin=None, stdout=None, stderr=None):
        try:
            self.stop()
        except RuntimeError:
            if require_stop:
                raise DaemonNotRunning()
        self.start(stdin, stdout, stderr)

    def __run(self):
        # Super usefull. (Not really)
        try:
            self.run()
        except Exception as e:
            raise

    def run(self):
        raise NotImplementedError("When subclassing a daemon you should override the run() method.")

    def dying(self):
        try: self.__delpid()
        except: pass


class UnixProxyDaemon(Daemon):
    """A daemon that uses json over Unix Domain Sockets to proxify \
    calls to methods of an object."""

    def __init__(self, pidfile_path, sockaddr_path, proxied_object):
        self.proxied = proxied_object
        self.sockaddr = sockaddr_path

        Daemon.__init__(self, pidfile_path)

    def __exec(self, cmd):
        if cmd[0] == "CALL":
            return ["RET", getattr(self.proxied, cmd[1])(*cmd[2], **cmd[3])]
        elif cmd[0] == "PING":
            return ["PONG"] + cmd[1:]
        else:
            raise LookupError("Unknown command <%s>." % cmd[0])

    def run(self):

        try:
            os.unlink(self.sockaddr)
        except OSError:
            if os.path.exists(self.sockaddr):
                raise

        self.__server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

        self.__server.bind(self.sockaddr)

        self.__server.listen(1)

        while True:

            connection, client_address = self.__server.accept()

            try:
                rd = ""
                while True:
                    data = connection.recv(16)
                    rd += data
                    if data[-1] == "\n":
                        break

                cmd = json.loads(rd.strip())

                sys.stdout.flush()

                try:
                    ret = self.__exec(cmd)
                except Exception as e:
                    import traceback
                    traceback.print_exc()
                    print >>sys.stderr, e
                    ret = ["ERR", str(e)]
                finally:
                    print >>sys.stderr, "End of __exec on stderr " + "-" * 60
                    print >>sys.stdout, "End of __exec on stdout " + "-" * 60
                    sys.stderr.flush()
                    sys.stdout.flush()

                jret = json.dumps(ret)

                connection.sendall(jret + "\n")

            finally:
                connection.close()

        # should not happen
        exit(0)


class UnixProxyClient(object):
    """A client for the UnixProxyDaemon. Allows for calls to the \
    proxyfied object."""

    class __Callable(object):
        """A callable object used by UnixProxyClient."""

        def __init__(self, client, method):
            self.client = client
            self.method = method

        def __call__(self, *args, **kwargs):
            return self.client.__call__(self.method, *args, **kwargs)

    def __init__(self, sockaddr_path, proxied_class):
        self.__clazz = proxied_class
        self.__sockaddr = sockaddr_path

    def __send_cmd(self, cmd):

        try:
            server = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            server.connect(self.__sockaddr)
        except socket.error as e:
            raise DaemonNotRunning()

        try:
            server.sendall(json.dumps(cmd) + "\n")
            retdata = ""
            while True:
                data = server.recv(16)
                retdata += data
                if data[-1] == "\n":
                    break
        except socket.error as e:
            raise DaemonNotRunning()
        finally:
            server.close()

        return json.loads(retdata.strip())


    def __getattribute__(self, name):
        try:
            return object.__getattribute__(self, name)
        except AttributeError:
            pass

        attr = getattr(object.__getattribute__(self, "_UnixProxyClient__clazz"), name)
        if not callable(attr):
            raise TypeError("'%s' object is not callable, thus not proxifiable." % type(attr))
        return UnixProxyClient.__Callable(self, name)

    def __call__(self, method, *args, **kwargs):
        #TODO: use __send_command() instead of this.
        # That should be used in testProxyClient as well to handle all the netwok stuff
        cmd = ["CALL", method, args, kwargs]

        ret = self.__send_cmd(cmd)

        if ret[0] == "ERR":
            raise RuntimeError("%s" % ret[1])

        return ret[1]

    def testProxyClient(self):
        _ = self.__send_cmd(["PING"])

